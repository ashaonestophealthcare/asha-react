
import { Button } from '../../../components/button';

const contentOne = [
    'Swab PCR 12-14 Jam',
    'Swab Antigen Plus by Abbott',
    'Swab Antigen Regular'
]
const contentTwo = [
    'Swab PCR 12-14 Jam',
    'Swab Antigen Plus by Abbott',
    'Swab Antigen Regular',
    'Vitamin C + Neurobion Injection',
    'Vitamin C + Collagen Injection',
    'Cernevit Multivitamin Booster',
    '& More'
]


export default function () {
    return (
        <div className="mt-4 py-4 mx-auto max-w-7xl"  id="service">
            <h1 className="text-3xl font-bold text-green-900 text-center">
                <u>
                    Layanan Kami
                </u>
            </h1>
            
            <div className="grid grid-cols-1 md:grid-cols-3 xl:grid-cols-3 mt-4">
                
                <div className="p-4">
                    <div className="bg-green-100 text-center">
                        <img
                            className="w-auto"
                            src="/images/service-1.png"
                            alt=""
                        />
                        <h4 className="text-center py-4 text-xl font-bold">Drive Thru / Walk In</h4>
                        <div className="h-64">
                        {
                            contentOne.map(i => (
                                <div className=" px-6 mb-2">
                                    <div>
                                        {i}
                                    </div>
                                </div>
                            ))
                        }
                        </div>
                       
                        <Button
                            title="Daftar Sekarang"
                            className="mb-6 mt-2"
                            onClick={()=> window.location.assign("https://form.jotform.com/212530783485459")}
                        />
                    </div>
                </div>


                <div className="p-4 ">
                    <div className="bg-green-100 text-center ">
                        <img
                            className="w-auto"
                            src="/images/service-2.png"
                            alt=""
                        />
                        <h4 className="text-center py-4 text-xl font-bold">Homecare / Corporate Service</h4>
                        <div className="h-64">
                        {
                            contentTwo.map(i => (
                                <div className=" px-6 mb-2">
                                    <div>
                                        {i}
                                    </div>
                                
                                </div>
                            ))
                        }
                        </div>
          
                        <Button
                            title="Daftar Sekarang"
                            className="mb-6 mt-2 text-center"
                            onClick={()=> window.location.assign("https://wa.me/message/CD7663LFMKE3I1")}
                        />
                    </div>
                </div>

                <div className="p-4">
                    <div className="bg-green-100 text-center">
                        <img
                            className="w-auto"
                            src="/images/service-3.png"
                            alt=""
                        />
                        <div className="px-8">
                            <h4 className="text-center py-4 text-xl font-bold">ASHA Clinic</h4>
                            <div className=" h-64">
                                <div>
                                    Konsultasi kesehatan dengan pendekatan <i>lifestyle medicine </i>yang mengatasi akar penyebab suatu penyakit dengan evidence-based therapy dalam intervensi perilaku gaya hidup seperti diet, olahraga, kualitas tidur, hubungan sosial, dan <i>stress management</i>
                                </div>
                            </div>
                            <div className="h-4"/>
                            <div className="mt-8 mb-6 text-green-100">
                                .
                            </div>
                          
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
    )
}
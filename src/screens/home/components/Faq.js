
import { Basic } from '../../../components/accordion';

const accordion = [{
    title:"Berapa lama hasil pemeriksaan Swab PCR & Antigen dapat keluar? ",
    description:"Hasil Swab PCR akan keluar dalam waktu 12 - 14 jam dari pengambilan sample. Hasil pemeriksaan Antigen akan keluar dalam waktu 15-30 menit."
},{
    title:"Apakah hasil pemeriksaan langsung terupload di Aplikasi Peduli Lindungi?",
    description:"Bersama mitra laboratorium kami, semua data hasil pemeriksaan akan diunggah ke website NAR setelah hasil keluar. Namun proses integrasi antara NAR dan Aplikasi Peduli Lindungi untuk penerbitan hasil ke dalam aplikasi di luar kapasitas fasilitas kesehatan."
},{
    title:"Apakah ada resiko keterlambatan hasil? ",
    description:"Sangat jarang terjadi. Namun, apabila terdapat pelonjakan kasus yang signifikan di waktu yang singkat dan untuk tetap menjaga kualitas, keterlambatan dapat terjadi. Ketepatan dan kecepatan hasil tetap adalah prioritas kami"
},{
    title:"Bagaimana pemeriksaan swab untuk anak-anak?",
    description:"Proses swab yang dilakukan kepada bayi dan anak-anak sudah sesuai SOP, karena dilakukan oleh tenaga medis yang berpengalaman dan dibawah pengawasan dokter. Kami menyediakan Antigen Kit Panbio Abbott Nasal dimana proses pengambilan sample hanya 2cm dari lubang hidung."
},{
    title:"Adakah kontraindikasi untuk infus Vitamin C? ",
    description:"Jika Anda memiliki penyakit ginjal, anemia, riwayat alergi berat dan penyakit berat lainnya, dapat diinformasikkan kepada team kami saat pemesanan. Infus vitamin C aman untuk lambung karena hampir 100% diserap langsung oleh pembuluh darah, sehingga tidak menyebabkan iritasi pada lambung. Prosedur dan dosis pemberian sudah sesuai SOP dan dilakukan oleh tenaga medis yang terlatih. "
},{
    title:"Apa keunggulan Antigen Plus dibandingkan dengan Antigen Regular?",
    description:"Layanan Antigen Plus menggunakan kit yang direkomendasikan WHO yaitu Panbio Abbott Nasal, dimana proses swab lebih nyaman, dengan tingkat akurasi yang sama tingginya dengan antigen reguler. Antigen reguler kami menggunakan alat yang telah tedaftar dan lulus uji validasi Kemenkes (AKL)"
},{
    title:"Apakah Saya akan mendapatkan hardcopy hasil pemeriksaan dan invoice?",
    description:"Hasil akan kami kirimkan melalui Whatsapp dalam format PDF. Hardcopy hasil pemeriksaan dan Invoice akan dikeluarkan by request di ASHA One-Stop Healthcare Epicentrum pada jam operasional."
},{
    title:"Bagaimana jika ingin melakukan pemeriksaan untuk kantor atau event tertentu?",
    description:"Sangat bisa! Anda dapat menghubungi Customer Service Home & Corporate Service di nomor: 087794480376, dan kami  dengan senang hati akan melayani sesuai kebutuhan Anda."
},{
    title:"Apakah harus melakukan pendaftaran online terlebih dahulu?",
    description:"Untuk mempercepat proses registrasi hingga pemeriksaan, dan mengurangi antrian di lokasi, kami sarankan untuk melakukan registrasi online terlebih dahulu. Namun, registrasi secara langsung di tempat juga tetap dapat dilakukan."
},{
    title:"Saya sudah daftar tapi belum mendapatkan konfirmasi melalui whatsapp maupun email, bagaimana?",
    description:"Bukti registrasi Anda terdapat di akhir halaman setelah Anda melakukan submit.  Anda dapat langsung datang ke lokasi swab kami tanpa mendapatkan konfirmasi pemesanan, tim kami akan melayani anda dengan senang hati."
}]

export default function () {
    return (
        <div className="mt-4 py-4 mx-auto max-w-7xl bg-bubble-group bg-no-repeat">
            <h1 className="text-3xl font-bold text-green-900 text-center">
                <u>
                    Sering Ditanyakan
                </u>
            </h1>
            <div className=" mx-auto mt-8 max-w-4xl">
            {
                accordion.map(i => (
                    <Basic 
                        title={i.title} 
                        titleClassName="font-bold text-green-900"
                        className="my-2" 
                        buttonClassName="bg-green-100 hover:bg-green-50" 
                    >
                        <div className="p-4 bg-yellow-150">
                            {i.description}
                        </div>
                    </Basic>
                ))
            }
            </div>
            
        </div>
    )
}
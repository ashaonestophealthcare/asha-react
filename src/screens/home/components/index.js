import AboutUs from './AboutUs';
import Advantages from './Advantages';
import Faq from './Faq';
import Footer from './Footer';
import Nav from './Nav';
import Head from './Head';
import Service from './Service';
import Sticky from './Sticky';

export {
    AboutUs,
    Advantages,
    Faq,
    Footer,
    Nav,
    Head,
    Service,
    Sticky
}
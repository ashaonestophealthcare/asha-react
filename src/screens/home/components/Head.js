import {Link} from 'react-scroll'

export default function () {
    return (
        <main className="lg:relative bg-hero-bubble bg-no-repeat bg-contain">
            <div className="mx-auto max-w-7xl w-full pt-16 pb-20 text-center lg:py-48 lg:text-left ">
                <div className="px-4 lg:w-1/2 sm:px-8 xl:pr-16 ">
                    <h3 className="text-md tracking-tight font-extrabold text-green-900 mb-8">
                        <span className="block md:inline">#OneAccessToHealthSuccess</span>{' '}
                    </h3>
                    <h1 className="text-xl tracking-tight font-extrabold text-green-900 sm:text-4xl md:text-3xl lg:text-3xl xl:text-4xl">
                        <span className="block xl:inline">Kemudahan Layanan Kesehatan</span>{' '}
                        <span className="block text-green-900 xl:inline">ASHA One-Stop Healthcare</span>
                    </h1>
                    <p className="mt-3 max-w-md mx-auto text-lg text-green-900 sm:text-xl md:mt-5 md:max-w-3xl">
                        Layanan pendukung peningkatan imun dan tes swab yang mudah, nyaman, terjangkau, dan berstandar internasional.
                    </p>
                    <div className="mt-10 sm:flex sm:justify-center lg:justify-start">
                        <div className="rounded-md shadow">
                            <a
                                href="#"
                                className="w-full flex items-center justify-center px-8 py-3 border border-transparent text-base font-medium rounded-md text-white bg-green-900 hover:bg-green-800 md:py-4 md:text-lg md:px-10"
                            >
                                <Link  to='service' spy={true} smooth={true}>
                                     Layanan Kami
                                </Link>  
                            </a>
                        </div>
                    </div>
                    <div className="grid grid-cols-3 mt-6 ">
                        <div className="border-r-4 p-4 border-green-700">
                            <div className="text-xl sm:text-3xl font-bold text-green-900 ">
                                25k +
                            </div>
                            <div className="text-xs sm:text-lg"> 
                                Swab PCR Test Customer
                            </div>
                        </div>

                        <div className="border-r-4 p-4 border-green-700">
                            <div className="text-xl sm:text-3xl font-bold text-green-900 ">
                                2k +
                            </div>
                            <div className="text-xs sm:text-lg"> 
                                Vitamin Booster Customer
                            </div>
                        </div>

                        <div className="p-4">
                            <div className="text-xl sm:text-3xl font-bold text-green-900 ">
                                15k +
                            </div>
                            <div className="text-xs sm:text-lg"> 
                                Coorporate Service Customer
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="w-full h-96 sm:h-72 md:h-96 lg:absolute lg:inset-y-0 lg:right-0 lg:w-1/2 lg:h-full bg-no-repeat bg-24 sm:bg-48 bg-header-person ">
                <div className="h-64 lg:h-96 "/>
                <div className="sm:h-36"/>
                
                <div className="grid grid-cols-1 md:grid-cols-3 pl-8 sm:pl-0 pr-8">

                    <div className="bg-white p-1 rounded mt-2  filter drop-shadow-lg flex flex-row items-center">
                        <img
                            className="w-auto h-24"
                            src="/images/hero-card-1.png"
                            alt=""
                        />
                        <div className="px-2">
                            <div className="text-xs font-bold">
                                Drive Thru & Walk In
                            </div>
                            <div className="border-b-2 border-green-700">

                            </div>
                            <div className="text-xs">
                                Swab PCR <br/>
                                Swab Antigen
                            </div>
                        </div>
           
                    </div>

                    <div className="bg-white p-1 rounded mt-2 sm:ml-2 filter drop-shadow-lg flex flex-row items-center ">
                        <img
                            className="w-auto h-24"
                            src="/images/hero-card-2.png"
                            alt=""
                        />
                        <div className="px-2">
                            <div className="text-xs font-bold">
                                Homecare & Corporate Service
                            </div>
                            <div className="border-b-2  border-green-700">

                            </div>
                            <div className="text-xs">
                                Swab PCR <br/>
                                Swab Antigen <br/>
                                Vitamin Booster
                            </div>
                        </div>
                    </div>

                    <div className="p-1 rounded mt-2 sm:ml-2 items-center">

                        <div className="flex flex-row bg-white filter p-2 drop-shadow-lg">
                            <img
                                className="w-auto h-8"
                                src="/images/nar-icon.png"
                                alt=""
                            />
                            <div className="text-xs pl-2 font-bold">
                                Integrated to NAR System
                            </div>
                        </div>

                        <div className="flex flex-row bg-white filter p-2 mt-2 drop-shadow-lg">
                            <img
                                className="w-auto h-8"
                                src="/images/wa-icon.png"
                                alt=""
                            />
                            <div className="text-xs pl-2 font-bold">
                                Whatsapp Results Notifications
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div className="h-64 sm:h-64 lg:h-0"/>
        </main>
    )
}

export default function () {
    return (
        <div className="bg-no-repeat bg-bubble-dark-group bg-32" id="aboutUs">
            <div className="mt-4 py-4 mx-auto max-w-7xl ">
                <h1 className="text-3xl mb-4 font-bold text-green-900 text-center">
                    <u>
                        Tentang Kami
                    </u>
                </h1>
                
                <div className="grid grid-cols-1 md:grid-cols-3 my-8">
                    <div className="col-span-1">
                        <img
                            className="h-128 mx-auto px-8"
                            src="/images/about-us.png"
                            alt=""
                        />
                    </div>
                    <div className="col-span-1 md:col-span-2 p-4 text-left">
                        <div className="text-3xl font-bold text-green-900">
                            ASHA One-Stop Healthcare
                        </div>
                        <div className="py-4 border-b-2  text-green-900 border-green-700">
                            ASHA One-Stop Healthcare adalah penyedia jasa layanan peningkatan Imunitas dan Test Covid-19 berstandar Internasional. ASHA One-Stop Healthcare menyediakan layanan yang secara sistem telah terintegrasi dengan Kementrian Kesehatan Republik Indonesia.
                        </div>
                        
                        <div className="mt-4 text-3xl font-bold text-green-900">
                            Visi
                        </div>
                        <div className="py-4 text-green-900">
                            Menjadi penyedia layanan kesehatan primer yang efisien, efektif dan berkualitas yang dapat menunjang peningkatan kualitas kesehatan masyarakat.
                        </div>

                        <div className="text-3xl font-bold text-green-900">
                            Misi
                        </div>
                        <div className="py-4 text-green-900">
                            Menyediakan sistem pelayanan kesehatan primer yang mudah diakses dari rumah maupun di fasilitas kesehatan ASHA One-Stop Healthcare
                        </div>
                        <div className="py-4 text-green-900">
                            Memberikan berbagai bentuk pelayanan kesehatan yang dapat memenuhi kebutuhan perkembangan masyarakat terkini secara mudah, cepat dan profesional.
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    )
}
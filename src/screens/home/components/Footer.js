
export default function () {
    return (
        <div className="py-4 bg-green-100">            
            <div className="mt-4 py-4 mx-auto max-w-7xl">     

                <div className="grid grid-cols-1 lg:grid-cols-3 space-x-8">

                    <div className="col-span-1 px-8">
                        <img
                            className="h-24"
                            src="/images/asha-logo.png"
                            alt=""
                        />

                        <div className="p-1 font-bold text-green-900">
                            Hubungi Kami
                        </div>
                        <div className="p-1 text-green-900">
                            +62 813-9838-4879
                        </div>
                        <div className="p-1 text-green-900">
                            info@ashahealthcare.id
                        </div>

                        <div className="pt-4 px-1 pb-1 font-bold text-green-900">
                            Jam Operasional
                        </div>
                        <div className="p-1 text-green-900">
                            Senin - Minggu | 08.00 s./d 18.00
                        </div>

                        <div className="pt-4 px-1 pb-1 font-bold text-green-900">
                            PT. Triasih Medika Apsara
                        </div>
                        <div className="p-1 text-green-900  pr-16">
                            Epiwalk Office Suites Lt. 6, Unit
                            A-601, Komplek Rasuna Epicentrum,
                            Jalan. H.R. Rasuna Said, RT.2/RW.5,
                            Kuningan, Karet Kuningan,
                            Kecamatan Setiabudi, Kota Jakarta
                            Selatan, Daerah Khusus Ibukota 
                            Jakarta 12940
                        </div>

                    </div>


                    <div className="col-span-1">
                        <div className="h-8 sm:h-24"/>


                        <div className="pt-1 px-1 pb-1 font-bold text-green-900 ">
                            Lokasi Kami
                        </div>
                        <div className="p-1 text-green-900 pr-16">
                            Epicircle - Kawasan Rasuna Epicentrum,
                            Jalan. H.R. Rasuna Said, RT.2/RW.5,
                            Kuningan, Karet Kuningan,
                            Kecamatan Setiabudi, Kota Jakarta
                            Selatan, Daerah Khusus Ibukota 
                            Jakarta 12940
                        </div>


                        <div className="invisible lg:visible">
                            <div className="flex flex-row space-x-4 mt-24">
                                <img
                                onClick={() => window.location.assign(` https://instagram.com/ashahealthcare.id?utm_medium=copy_link`)}
                                    className="h-6 w-auto rounded cursor-pointer"
                                    src="/images/socmed-ig.png"
                                    alt=""
                                />
                                 <img
                                    className="h-6 w-auto rounded cursor-pointer"
                                    src="/images/socmed-ln.png"
                                    alt=""
                                />
                                 <img
                                    className="h-6 w-auto rounded cursor-pointer"
                                    src="/images/socmed-fb.png"
                                    alt=""
                                />
                            </div>
                        </div>
                    </div>

                    <div className="col-span-1">
                        <div className="mt-0 pr-4 sm:mt-24 cursor-pointer" onClick={() => window.location.replace("https://goo.gl/maps/sdvgww4CFxGEetjA7")}>
                            <img
                                className="h-64 w-auto rounded"
                                src="/images/asha-google-map.jpeg"
                                alt=""
                            />
                        </div>

                        <div className="visible sm:invisible ">
                            <div className="flex flex-row space-x-4 mt-24 mx-auto">
                                <img
                                onClick={() => window.location.assign(` https://instagram.com/ashahealthcare.id?utm_medium=copy_link`)}
                                    className="h-6 w-auto rounded cursor-pointer"
                                    src="/images/socmed-ig.png"
                                    alt=""
                                />
                                 <img
                                    className="h-6 w-auto rounded cursor-pointer"
                                    src="/images/socmed-ln.png"
                                    alt=""
                                />
                                 <img
                                    className="h-6 w-auto rounded cursor-pointer"
                                    src="/images/socmed-fb.png"
                                    alt=""
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    )
}
/* This example requires Tailwind CSS v2.0+ */
import { Fragment } from 'react'
import { Popover, Transition } from '@headlessui/react'
import {
  BookmarkAltIcon,
  CalendarIcon,
  ChartBarIcon,
  CursorClickIcon,
  MenuIcon,
  PhoneIcon,
  PlayIcon,
  RefreshIcon,
  ShieldCheckIcon,
  SupportIcon,
  ViewGridIcon,
  XIcon,
} from '@heroicons/react/outline'
import { ChevronDownIcon } from '@heroicons/react/solid'
import {Link} from 'react-scroll'

const navigation = [
  { name: 'Utama', href: '#', to:'main' },
  { name: 'Layanan Kami', href: '#', to:'service' },
  { name: 'Keunggulan Kami', href: '#', to:'advantages' },
  { name: 'Tentang Kami', href: '#', to:'aboutUs' },
]


export default function () {
    return (
        <div className="relative bg-gray-50">
            <Popover as="header" className="relative z-10">
                <div className="bg-yellow-150 ">
                <nav
                    className="relative max-w-7xl mx-auto flex items-center justify-between py-2 px-6 xl:px-8"
                    aria-label="Global"
                >
                    <div className="flex items-center justify-between w-full lg:w-auto">
                    <a href="#">
                        <span className="sr-only">Asha</span>
                        <img
                            className="h-16 w-auto sm:h-10"
                            src="/images/asha-logo.png"
                            alt=""
                        />
                    </a>
                    <div className="-mr-2 flex items-center lg:hidden">
                        <Popover.Button className="bg-yellow-150 rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:bg-gray-100 focus:outline-none focus:ring-2 focus-ring-inset focus:ring-grape-500">
                            <span className="sr-only">Open main menu</span>
                            <MenuIcon className="h-6 w-6" aria-hidden="true" />
                        </Popover.Button>
                    </div>
                    </div>
                    <div className="hidden space-x-10 lg:flex lg:ml-10">
                    {navigation.map((item) => (
                        <a key={item.name} href={item.href} className="text-base font-medium ">
                            <Link  to={item.to} spy={true} smooth={true}>
                                {item.name}
                            </Link>
                        </a>
                    ))}
                    </div>
                    <div className="hidden lg:flex lg:items-center lg:space-x-6">
                    <a
                        onClick={() => window.location.assign('https://wa.me/message/INBUE2FATNBJG1')}
                        className="cursor-pointer py-2 px-6  text-base font-medium text-green-700 hover:bg-green-50"
                    >
                        Hubungi Kami
                    </a>
                    </div>
                </nav>
                </div>

                <Transition
                    as={Fragment}
                    enter="duration-150 ease-out"
                    enterFrom="opacity-0 scale-95"
                    enterTo="opacity-100 scale-100"
                    leave="duration-100 ease-in"
                    leaveFrom="opacity-100 scale-100"
                    leaveTo="opacity-0 scale-95"
                >
                    <Popover.Panel focus className="absolute top-0 inset-x-0 p-2 transition transform origin-top lg:hidden">
                        <div className="rounded-lg shadow-lg bg-white ring-1 ring-black ring-opacity-5 overflow-hidden">
                            <div className="px-5 pt-4 flex items-center justify-between">
                                <div>
                                <img
                                    className="h-16 w-auto"
                                    src="/images/asha-logo.png"
                                    alt=""
                                />
                                </div>
                                <div className="-mr-2">
                                <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-grape-500">
                                    <span className="sr-only">Close menu</span>
                                    <XIcon className="h-6 w-6" aria-hidden="true" />
                                </Popover.Button>
                                </div>
                            </div>
                            <div className="pt-5 pb-6">
                                <div className="px-2 space-y-1">
                                {navigation.map((item) => (
                                    <a
                                    key={item.name}
                                    href={item.href}
                                    className="block px-3 py-2 rounded-md text-base font-medium text-gray-900 hover:bg-gray-50"
                                    >
                                        <Link  to={item.to} spy={true} smooth={true}>
                                            {item.name}
                                        </Link>
                                    </a>
                                ))}
                                </div>
                                <div className="mt-6 px-5">
                                <a
                                    onClick={() => window.location.assign('https://wa.me/message/INBUE2FATNBJG1')}
                                    className="block cursor-pointer text-center w-full py-2 px-4 text-green-700 font-medium hover:bg-green-900"
                                >
                                    Hubungi Kami
                                </a>
                                </div>
                            </div>
                        </div>
                    </Popover.Panel>
                </Transition>
            </Popover>
        </div>
    )
}

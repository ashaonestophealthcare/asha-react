
import { Button } from '../../../components/button';

const contentOne = [{
    name:"Swab PCR 10 Jam",
    price:"Rp. 780.000"
},{
    name:"Swab PCR 10 Jam",
    price:"Rp. 780.000"
},{
    name:"Swab PCR 10 Jam",
    price:"Rp. 780.000"
},{
    name:"Swab PCR 10 Jam",
    price:"Rp. 780.000"
},{
    name:"Swab PCR 10 Jam",
    price:"Rp. 780.000"
}]

const contentTwo = [
    'Lorem ipsum 1234',
    'Lorem ipsum 1234',
    'Lorem ipsum 1234',
    'Lorem ipsum 1234',
    'Lorem ipsum 1234'
]

export default function () {
    return (
        <div className="mt-4 py-4 mx-auto max-w-7xl" id="advantages">
            <h1 className="text-3xl font-bold text-green-900 text-center">
                <u>
                    Keunggulan Kami
                </u>
            </h1>
            
            <div className="grid grid-cols-2 lg:grid-cols-4 xl:grid-cols-4 mt-4">
                
                <div className="p-4">
                    <div className="center">
                        <img
                            className="h-24 sm:h-36 mx-auto"
                            src="/images/pin-icon-bg.png"
                            alt=""
                        />
                        <h4 className="text-center text-md sm:text-xl font-bold text-green-900 -mt-4">
                            Kemudahan Lokasi
                        </h4>
                        <div className="text-centertext-green-900 px-4 py-2 text-sm sm:text-base text-center">
                            Lokasi strategis yang mudah dijangkau
                        </div>
                    </div>
                </div>

                <div className="p-4">
                    <div className="">
                        <img
                            className="h-24 sm:h-36 mx-auto"
                            src="/images/badge-icon-bg.png"
                            alt=""
                        />
                        <h4 className="text-center text-md sm:text-xl font-bold text-green-900 -mt-4">
                            Harga dan Mutu
                        </h4>
                        <div className="text-centertext-green-900 px-4 py-2 text-sm sm:text-base text-center">
                            Kualitas harga yang disertai dengan mutu pada setiap layanan kami
                        </div>
                    </div>
                </div>

                <div className="p-4">
                    <div className="">
                        <img
                            className="h-24 sm:h-36 mx-auto"
                            src="/images/hospital-icon-bg.png"
                            alt=""
                        />
                        <h4 className="text-center text-md sm:text-xl font-bold text-green-900 -mt-4">
                            Layanan Homecare
                        </h4>
                        <div className="text-centertext-green-900 px-4 py-2 text-sm sm:text-base text-center">
                            Jangkauan layanan homecare yang cukup besar demi kenyamanan pasien
                        </div>
                    </div>
                </div>

                <div className="p-4">
                    <div className="">
                        <img
                            className="h-24 sm:h-36 mx-auto"
                            src="/images/atom-icon-bg.png"
                            alt=""
                        />
                        <h4 className="text-center text-md sm:text-xl font-bold text-green-900 -mt-4">
                            Pelayanan dan Teknologi
                        </h4>
                        <div className="text-centertext-green-900 px-4 py-2 text-sm sm:text-base text-center">
                            Pelayanan yang dinamis mengikuti perkembangan pasien dan teknologi kesehatan
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    )
}
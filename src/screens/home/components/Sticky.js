import { Container } from 'react-floating-action-button';

export default function () {
    return (
        <Container>
               <img
                    onClick={() => window.location.assign('https://wa.me/message/INBUE2FATNBJG1')}
                    className="h-18 w-auto rounded cursor-pointer"
                    src="/images/whatsapp-icon.png"
                    alt=""
                />
        </Container >
    )
}
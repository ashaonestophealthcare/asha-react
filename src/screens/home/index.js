import { AboutUs, Advantages, Faq, Footer, Nav, Head, Service, Sticky  } from './components';

export default function () {
    return (
        <div>
            <Nav/>
            <Head/>
            <Service/>
            <Advantages/>
            <Faq/>
            <AboutUs/>
            <Footer/>
            <Sticky/>
        </div>
    )
}

export default function (props) {
    const handleSubmit = async (e) => {
        e.preventDefault();
        props.onSubmit();
        return
    }


    return (
        <form action="#" method={props.method} onSubmit={handleSubmit}>
            {props.children}
        </form>
    )
}
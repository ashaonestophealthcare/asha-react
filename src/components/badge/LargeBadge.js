/* This example requires Tailwind CSS v2.0+ */
export default function (props) {
    let className="";
    switch (props.type) {
        case 'secondary':
            className="inline-flex items-center px-3 py-0.5 rounded-full text-sm font-medium bg-gray-100 text-gray-800"
            break;
        case 'danger':
            className="inline-flex items-center px-3 py-0.5 rounded-full text-sm font-medium bg-red-100 text-red-800"
            break;
        case 'warning':
            className="inline-flex items-center px-3 py-0.5 rounded-full text-sm font-medium bg-yellow-100 text-yellow-800"
            break;
        case 'success':
            className="inline-flex items-center px-3 py-0.5 rounded-full text-sm font-medium bg-green-100 text-green-800"
            break;
        case 'info':
            className="inline-flex items-center px-3 py-0.5 rounded-full text-sm font-medium bg-blue-100 text-blue-800"
            break;
        case 'infoLight':
            className="inline-flex items-center px-3 py-0.5 rounded-full text-sm font-medium bg-indigo-100 text-indigo-800"
            break;
        case 'default':
            className="inline-flex items-center px-3 py-0.5 rounded-full text-sm font-medium bg-purple-100 text-purple-800"
            break;
        case 'pink':
            className="inline-flex items-center px-3 py-0.5 rounded-full text-sm font-medium bg-pink-100 text-pink-800"
            break;
        default:
            break;
    }
    return (
        <span className={className}>
            {props.title}
        </span>
    )
}
  
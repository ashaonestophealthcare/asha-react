import LargeBadge from './LargeBadge';
import SmallBadge from './SmallBadge';

export {
    LargeBadge,
    SmallBadge
}
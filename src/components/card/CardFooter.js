import { Button } from '../button'

export default function (props) {
    return (
        <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
            <Button
                loading={props.loadingPrimary}
                disabled={props.loadingPrimary}
                className=""
                title="Save"
                onClick={props.onClickPrimary}
            />
        </div>
    )
  }

export default function (props) {
    return (
        <div className={`${props.className} bg-white py-6 px-4 sm:p-6`}>  
            {props.children}
        </div>
    )
  }
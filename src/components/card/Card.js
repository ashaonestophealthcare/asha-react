
export default function (props) {
  return (
    <div className={`${props.className} bg-white overflow-hidden shadow rounded-lg`}>
      {props.children}
    </div>
  )
}

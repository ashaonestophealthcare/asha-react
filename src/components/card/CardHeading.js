export default function (props) {
    let borderClassName;
    if (props.border) {
        borderClassName=`border-b py-5`
    } else {
        borderClassName=`pt-5`
    }
    return (
        <div className={`bg-white px-4 sm:px-6 ${borderClassName}`}>
            <h3 className="text-lg leading-6 font-medium text-gray-900">
                {props.title}
            </h3>
            <p className="mt-1 text-sm text-gray-500">
                {props.description}
            </p>
        </div>
    )
  }
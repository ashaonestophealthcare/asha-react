import { XCircleIcon } from '@heroicons/react/solid'

export default function (props) {
    let typeBg="";
    switch (props.type) {
        case 'secondary':
            typeBg="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-gray-100 text-gray-800"
            break;
        case 'danger':
            typeBg=""
            break;
        case 'warning':
            typeBg="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-yellow-100 text-yellow-800"
            break;
        case 'success':
            typeBg="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800"
            break;
        case 'info':
            typeBg="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-blue-100 text-blue-800"
            break;
        case 'infoLight':
            typeBg="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-indigo-100 text-indigo-800"
            break;
        case 'default':
            typeBg="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-purple-100 text-purple-800"
            break;
        case 'pink':
            typeBg="inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-pink-100 text-pink-800"
            break;
        default:
            break;
    }

    return (
        <div className={`${typeBg} ${props.className}`}>
            <div className="flex">
                <div className="flex-shrink-0">
                    <XCircleIcon className="h-5 w-5 text-red-400" aria-hidden="true" />
                </div>
                <div className="ml-3">
                    <h3 className="text-sm font-medium text-red-800">{props.description}</h3>
                </div>
            </div>
        </div>
    )
}
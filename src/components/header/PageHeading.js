import { Button } from '../button';

export default function (props) {
    return (
        <div className="flex items-center md:justify-between">
            <div className="flex-1 min-w-0">
                <h2 className="font-semibold leading-7 text-gray-900 text-2xl sm:truncate">
                    {props.title}
                </h2>
            </div>
            {
                !props.buttonTitle || props.buttonHidden ? null :
                    <div className="flex md:ml-4">
                        <Button
                            title={props.buttonTitle}
                            className="ml-3"
                            onClick={props.onClickButton}
                        />
                    </div>
            }
        </div>
    )
  }
  
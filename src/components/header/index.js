import PageHeading from './PageHeading';
import Title from './Title';

export {
    PageHeading,
    Title
}
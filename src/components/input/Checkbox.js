
export default function (props) {
    return (
        <div className={props.className}>
            <input
                id={props.name}
                aria-describedby="comments-description"
                name={props.name}
                checked={props.checked}
                onChange={props.onChange}
                type="checkbox"
                className="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded"
            />
        </div>
    )
}
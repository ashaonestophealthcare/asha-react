import React from 'react';
import { ExclamationCircleIcon } from '@heroicons/react/solid'

export default function (props) {
    return (
        <div className={props.className}>
            { 
                props.labelHidden ? null :
                    <label htmlFor="about" className="block text-sm font-medium text-gray-700">
                        {props.label}
                    </label>
            }
          
            <div className="mt-1">
                <textarea
                    id={props.name}
                    name={props.name}
                    value={props.value}
                    rows={props.rows}
                    onChange={props.onChange}
                    className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md"
                    placeholder={props.placeholder}
                    defaultValue={props.defaultValue}
                />
            </div>
        </div>
    )
}

import React, { Component } from 'react';

import CreatableSelect from 'react-select/creatable';

const components = {
  DropdownIndicator: null,
};

const createOption = (label) => ({
  label,
  value: label,
});

export default class CreatableInputOnly extends Component {
  state = {
    inputValue: '',
    value: [],
    arrayValue:[]
  };

  componentDidMount = () => { 
    let arrayValue = this.state.value;
    if (this.props.value) {
      arrayValue = this.props.value.reduce((r,i) => {r.push(i.value);return r},[]);
    } 
    this.setState({ 
      value:this.props.value,
      arrayValue:arrayValue
     });
  };

  handleChange = (value, actionMeta) => {
    const arrayValue = value.reduce((r,i) => {r.push(i.value);return r},[]);
    this.setState({ value,arrayValue });
    this.props.onChange([...value])
  };
  
  handleInputChange = (inputValue) => {
    this.setState({ inputValue });
  };

  handleKeyDown = (event) => {
    const { inputValue, value } = this.state;
    if (!inputValue) return;
    switch (event.key) {
      case 'Enter':
      case 'Tab':
        if (this.state.arrayValue.includes(inputValue)){
          return alert('Sudah ada kata kunci yang sama');
        }
        const newValue = [...value, createOption(inputValue)];
        const arrayValue = newValue.reduce((r,i) => {r.push(i.value);return r},[]);
        this.setState({
          inputValue: '',
          value:newValue,
          arrayValue: arrayValue
        });
        this.props.onChange([...value, createOption(inputValue)])
        event.preventDefault();
    }
  };
  
  render() {
    const { inputValue, value } = this.state;

    return (
      <div className={this.props.className}>
        <CreatableSelect
          components={components}
          inputValue={inputValue}
          isClearable
          isMulti
          menuIsOpen={false}
          onChange={this.handleChange}
          onInputChange={this.handleInputChange}
          onKeyDown={this.handleKeyDown}
          placeholder={this.props.placeholder}
          value={this.props.value}
        />
      </div>
    );
  }
}
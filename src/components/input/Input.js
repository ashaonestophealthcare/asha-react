import React from 'react';
import { ExclamationCircleIcon } from '@heroicons/react/solid'
import { SearchIcon } from '@heroicons/react/solid'

export default function (props) {
    const addOns = props.addOns || [];
    let inputClassName,leadingComponent,trailingComponent;
    if (props.alert) {
        inputClassName = `block w-full pr-10 border-red-300 text-red-900 placeholder-red-300 focus:outline-none focus:ring-red-500 focus:border-red-500 sm:text-sm rounded-md`
    } else {
        inputClassName = `appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm`
    }

    if (addOns.includes('leading')) {
        inputClassName = `pl-7 ${props.leadingClassName} appearance-none block w-full py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm`
        leadingComponent = (
            <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                <span className="text-gray-500 sm:text-sm">{props.leadingText || "IDR"}</span>
            </div>
        )
    } else if (addOns.includes('trailing')) {
        inputClassName = ` pl-3 pr-12 ${props.trailingClassName} appearance-none block w-full py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm`
        trailingComponent = (
            <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                <span className="text-gray-500 sm:text-sm" id="price-currency">
                    {props.trailingText || "g"}
                </span>
            </div>
        )
    } else if (addOns.includes('icon')) {
        inputClassName = `focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-10 sm:text-sm border-gray-300 rounded-md`
        let icon;
        switch (props.iconName) {
            case 'search':
                icon = <SearchIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />;
                break;
            default:
                break;
        }
        leadingComponent = (
            <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                {icon}
            </div>
        )
    }
    return (
        <div className={props.className}>
            { 
                props.labelHidden ? null :
                    <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                        { props.label }
                    </label>
            }
          
            
            <div className={`${props.labelHidden ? "" : "mt-1"} relative rounded-md shadow-sm"`}>
                { leadingComponent }   
                <input
                    type={props.type || "text"}
                    name={props.name}
                    id={props.id}
                    className={inputClassName}
                    placeholder={props.placeholder}
                    onChange={props.onChange}
                    value={props.value}
                    defaultValue=""
                    aria-invalid="true"
                    aria-describedby="email-error"
                />
                { trailingComponent }
                <div className="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
                {
                    props.alert ? 
                        <ExclamationCircleIcon className="h-5 w-5 text-red-500" aria-hidden="true" /> : null
                }
                
                </div>
            </div>
            {
                props.alert ? 
                <p className="mt-2 text-sm text-red-600" id="email-error">
                    {props.alert}
                </p> : null
            }
            
        </div>
    )
}

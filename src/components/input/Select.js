import React, { useState } from 'react';

export default function (props) {
    return (
        <div className={props.className}>
            { 
                props.labelHidden ? null :
                    <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                        { props.label }
                    </label>
            }
            <select
                className="mt-1 block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md"
                onChange={props.onChange}
                value={props.value}
            >
                {
                    props.options.map(i => (
                        <option selected={i.value == props.value} value={i.value}>{i.label}</option>
                    ))
                }
            </select>
        </div>
    )
  }
  
import React, { Component } from 'react';
import PlacesAutocomplete, {
  geocodeByPlaceId,
  getLatLng,
} from 'react-places-autocomplete';

export default class InputLocation extends Component {

  constructor(props) {
    super(props);
    this.state = {
      coordinate: {},
      locations: [],
      loading:false,
      editMap:false,
      editGeo:false
    };
  }

  handleGeoChange = address => {
    this.props.onChange(address);
  };

  handleMapChange = async e => {
    this.setState({
      address: e.target.value,
      loading: true
    });
  };

  handleGeoSelect = async (address,placeId) => {
      try {
          let geocode = await geocodeByPlaceId(placeId);
          let latLng = await getLatLng(geocode[0]);
          let coordinate = {
              latitude:latLng.lat,
              longitude:latLng.lng
          };

          const resPostal = geocode[0].address_components.filter(i => i.types[0] === 'postal_code');
          const postalCode = resPostal.length === 0 ? '-' : resPostal[0].long_name;
          this.setState({
              address,
              coordinate
          });
          this.props.onSelect({address, coordinate, placeId, postalCode});

      } catch (e) {
          console.log (e);
      }
  };

  render() {
    const searchOptions = {
        componentRestrictions: { country: ['id'] }
    };

    return (
        <PlacesAutocomplete
            searchOptions={searchOptions}
            value={this.props.value}
            onChange={this.handleGeoChange}
            onSelect={this.handleGeoSelect}
            debounce={750}
        >
            {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
                <div className="">
                       { 
                            this.props.labelHidden ? null :
                                <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                                    { this.props.label }
                                </label>
                        }
          
                    <input
                        {...getInputProps({
                        placeholder: this.props.placeholder,
                        className: `${this.props.loading ? 'text-muted' :''} mt-1 appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm`,
                        })}
                        value={this.props.loading ?
                            this.props.loadingText ? this.props.loadingText : "Loading..."
                            : this.props.value
                        }
                        required
                    />
                    <div
                        className={`absolute z-10 w-max mt-2 bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm`}
                        style={{position:'absolute', zIndex:99 }}>
                    {
                        loading && <div className="absolute z-10 mt-2 bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm">Loading...</div>
                    }
                    {
                        suggestions.map(suggestion => {
                            const className = suggestion.active
                                ? 'suggestion-item--active'
                                : 'suggestion-item';
                            // inline style for demonstration purpose
                            const style = suggestion.active
                                ? { backgroundColor: '#fafafa', cursor: 'pointer', zIndex:99  }
                                : { backgroundColor: '#ffffff', cursor: 'pointer', zIndex:99  };
                            return (
                                <div
                                    {...getSuggestionItemProps(suggestion, {
                                    className,
                                    style
                                    })}
                                    className="px-4 pb-2 suggestion-item"
                                >
                                    <span className="text-sm text-gray-700 font-bold">
                                        {suggestion.formattedSuggestion.mainText}
                                    </span>
                                    <br/>
                                    <span className="text-sm text-gray-700">
                                        {suggestion.formattedSuggestion.secondaryText}
                                    </span>
                                </div>
                            );
                        })
                    }
                    </div>
                </div>
            )}
            </PlacesAutocomplete>
        )
    };
}


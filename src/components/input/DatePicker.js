import React, { useState } from 'react';
import moment from 'moment';
import DatePicker from "react-datepicker";

export default function (props) {
    let someDate = new Date();
    const multiplier = 86400000;
    const [ date, setDate ] = useState(new Date(new Date().getTime() + parseInt(1*multiplier)))

    const onSetDate = (d) => {
        setDate(d)
        props.onChange(d)    
    };

    return (
      <div>
        { 
            props.labelHidden ? null :
                <label htmlFor="email" className="block text-sm font-medium text-gray-700">
                    { props.label }
                </label>
        }
        <div className="mt-1 rounded-md shadow-sm z-99">
          <DatePicker
              className="form-control mb-0"
              onChange={e => onSetDate(e)}
              selected={date}
              nextMonthButtonLabel=">"
              previousMonthButtonLabel="<"
          />
        </div>
      </div>
      
    )
}
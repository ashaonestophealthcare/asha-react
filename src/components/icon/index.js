import Icon from './Icon';
import RoundedIcon from './RoundedIcon';

export {
    Icon,
    RoundedIcon
}
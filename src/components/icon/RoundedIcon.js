import { CheckIcon, ThumbUpIcon, UserIcon } from '@heroicons/react/solid'

export default function (props) {
    let icon;
    switch (props.icon) {
        case 'check':
            icon = <CheckIcon className="h-5 w-5 text-white" aria-hidden="true" />
            break;
        default:
            break;
    }
    return (
        <span className={`${props.iconBackground} h-8 w-8 rounded-full flex items-center justify-center ring-8 ring-white`}>
            {icon}
        </span>
    )
}
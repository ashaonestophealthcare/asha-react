import { CheckIcon, ThumbUpIcon, UserIcon, ArrowRightIcon } from '@heroicons/react/solid'

export default function (props) {
    let icon;
    switch (props.icon) {
        case 'check':
            icon = <CheckIcon className="h-5 w-5 text-gray-900" aria-hidden="true" />
            break;
        case 'arrowRight':
            icon = <ArrowRightIcon className="h-4 w-4 text-gray-900" aria-hidden="true" />
            break;
        default:
            break;
    }
    return (
        <span className={`${props.iconBackground} h-8 w-8 flex items-center justify-center `}>
            {icon}
        </span>
    )
}
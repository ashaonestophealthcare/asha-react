import ModalSuccess from './Success';
import ModalHeading from './Heading';
import ModalFooter from './Footer';
import Modal from './Modal';
export {
    Modal,
    ModalFooter,
    ModalHeading,
    ModalSuccess
}
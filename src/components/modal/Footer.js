import { Button } from '../button';

export default function (props) {
    return (
        <div className={`${props.className} border-t-2 flex flex-end flex-row items-center p-4 justify-end space-x-4`}>
            {
               !props.secondaryButtons ? null : !props.secondaryButtons.length > 0 ? null :
                    props.secondaryButtons.map(i => (
                        <Button
                            secondary={true}
                            title={i.title}
                            onClick={i.onClick}
                        />
                    ))
            }
            {
                !props.primaryButton ? null :
                    <Button 
                        title={props.primaryButton.title}
                        onClick={props.primaryButton.onClick}
                    />
            }
        </div>
    )
}
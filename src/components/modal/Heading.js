import { ButtonIcon } from '../button';

export default function (props) {
    return (
        <div className={`${props.className} border-b-2 flex flex-row items-center space-x-96 p-4`}>
            <h3 className="text-lg leading-6 font-medium text-gray-900">
                {props.title}
            </h3>
            <ButtonIcon 
                border={false}
                onClick={props.onClose}
                icon="x"
            />
        </div>
    )
}
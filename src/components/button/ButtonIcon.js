import { BellIcon, TrashIcon, UserIcon, PlusIcon, XIcon } from '@heroicons/react/outline'

export default function (props) {
    let icon;
    switch (props.icon) {
        case 'bell':
            icon = <BellIcon className="h-6 w-6" aria-hidden="true" />
            break;
        case 'trash':
            icon = <TrashIcon className="h-5 w-5" aria-hidden="true" />
            break;
        case 'plus':
            icon = <PlusIcon className="h-5 w-5" aria-hidden="true" />
            break;
        case 'x':
            icon = <XIcon className="h-5 w-5" aria-hidden="true" />
            break;
        default:
            break;
    }

    let borderClass = "border border-gray-300 shadow-sm";
    if (props.border === false) {
        borderClass = null
    }
    
    return (
        <div className={props.className}>
            <button
                type="button"
                className={`${borderClass} py-2 px-2 justify-center text-sm font-medium rounded-md text-gray-500 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 ${props.buttonClassName}` }
                onClick={props.onClick}
            >
                {icon}
            </button>
        </div>
    )
}
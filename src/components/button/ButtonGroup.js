import { DropdownButton } from '../dropdown';
/* This example requires Tailwind CSS v2.0+ */
export default function (props) {
    const middleButtons = props.buttons.slice(1,-1);
    return (
        <span className="relative z-0 inline-flex shadow-sm rounded-md">
            {
                props.buttons[0].dropdown ? 
                    <DropdownButton
                        customButtonClassName
                        title={props.buttons[0].title}
                        ButtonClassName="-ml-px relative inline-flex items-center px-4 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
                        actions={props.buttons[0].actions}
                    /> :
                    <button
                        type="button"
                        className="relative inline-flex items-center px-4 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
                        onClick={props.buttons[0].onClick}
                    >
                        {props.buttons[0].title}
                    </button>
            }
      

            {
                props.buttons.length > 2 ? 
                <div className="flex flex-row">
                    {
                        middleButtons.map(i => (
                            <button
                                type="button"
                                className="-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
                                onClick={i.onClick}
                            >
                                {i.title}
                            </button>
                        ))
                    }
                </div>
                :
                null
            }
            {
                props.buttons[props.buttons.length-1].dropdown ? 
                    <DropdownButton
                        customButtonClassName
                        title={props.buttons[props.buttons.length-1].title}
                        ButtonClassName="-ml-px relative inline-flex items-center px-4 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
                        actions={props.buttons[props.buttons.length-1].actions}
                    /> :
                    <button
                        type="button"
                        className="-ml-px relative inline-flex items-center px-4 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:z-10 focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500"
                        onClick={props.buttons[props.buttons.length-1].onClick}
                    >
                        {props.buttons[props.buttons.length-1].title}
                    </button>
            }
       

        </span>
    )
}
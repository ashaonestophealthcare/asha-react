
export default function ( props ) {
    let defaultClassName;
    if (props.secondary) {
        defaultClassName = `inline-flex items-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-500`
    } else {
        defaultClassName = `inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-green-900 hover:bg-green-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-700`
    }
    return ( 
        <div>
            <button
                type="button"
                className={`${props.className} ${defaultClassName}`}
                onClick={props.onClick}
                disabled={props.loading}
            >
                {
                    !props.loading ? null :
                        <svg className="animate-spin h-5 w-5 mr-3" viewBox="0 0 24 24"></svg>
                }
                {props.loading ? "Loading..." : props.title}
            </button>
        </div>
    )
  }
  
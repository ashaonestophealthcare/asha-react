
export default function (props) {
    return (
        <div className="overflow-hidden">
            <div className="border-t border-gray-200 px-4 py-5 sm:p-0">
                <dl className="sm:divide-y sm:divide-gray-200">
                    {
                        props.list.map(i => {
                            return (
                                <div className="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                    <dt className="text-sm font-medium text-gray-500">
                                        {i.title}
                                    </dt>
                                    <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                        {
                                            i.descriptionComponent ||
                                            <div className={i.descriptionClassName}>
                                                {i.description}
                                            </div>
                                        }
                                        
                                    </dd>
                                </div>
                            )
                        })
                    }
                </dl>
            </div>
        </div>
    )
}

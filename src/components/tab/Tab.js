
const tabs = [
    { name: 'Applied', href: '#', count: '52', current: false },
    { name: 'Phone Screening', href: '#', count: '6', current: false },
    { name: 'Interview', href: '#', count: '4', current: true },
    { name: 'Offer', href: '#', current: false },
    { name: 'Disqualified', href: '#', current: false },
  ]
  
  function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
  }
  
export default function (props) {
    return (
        <div>
            <div className="sm:hidden p-4 border-b border-gray-200">
                <label htmlFor="tabs" className="sr-only">
                    Select a tab
                </label>
                {/* Use an "onChange" listener to redirect the user to the selected tab URL. */}
                <select
                    id="tabs"
                    name="tabs"
                    className="block w-full pl-3 pr-10 py-2 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md"
                    onChange={e => props.onSelect(e.target.value)}
                    defaultValue={props.tabs[1].value}
                >
                    {props.tabs.map((tab) => (
                    <option key={tab.value} value={tab.value}>{tab.title}</option>
                    ))}
                </select>
            </div>
            <div className="hidden sm:block">
                <div className="border-b border-gray-200 px-4">
                    <nav className="-mb-px flex space-x-8" aria-label="Tabs">
                    {props.tabs.map((tab) => (
                        <a
                            key={tab.value}
                            onClick={() => props.onSelect(tab.value)}
                            className={classNames(
                                tab.current
                                ? 'border-indigo-500 text-indigo-600 cursor-pointer'
                                : 'border-transparent text-gray-500 cursor-pointer hover:text-gray-700 hover:border-gray-200',
                                'whitespace-nowrap flex py-4 px-1 border-b-2 font-medium text-sm'
                            )}
                            aria-current={tab.current ? 'page' : undefined}
                        >
                        {tab.title}
                        {tab.count ? (
                            <span
                                className={classNames(
                                    tab.current ? 'bg-indigo-100 text-indigo-600' : 'bg-gray-100 text-gray-900',
                                    'hidden ml-3 py-0.5 px-2.5 rounded-full text-xs font-medium md:inline-block'
                                )}
                            >
                            {tab.count}
                            </span>
                        ) : null}
                        </a>
                    ))}
                    </nav>
                </div>
            </div>
        </div>
    )
}
  
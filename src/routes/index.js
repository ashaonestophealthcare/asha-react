import React from 'react';
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';
import Home from '../screens/home';

// Routes
import PublicRoute from './PublicRoute';

export default function () {
    return (
        <BrowserRouter>
            <Switch>
                <PublicRoute path="/" exact component={Home} />
                <Redirect to="/" />
            </Switch>
        </BrowserRouter>
    );
};


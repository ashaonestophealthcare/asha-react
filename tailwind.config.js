module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      'display': ['"Nunito Sans"', 'sans-serif'],
      'body': ['"Nunito Sans"', 'sans-serif'],
    },
    extend: {
      backgroundImage: {
        'hero-bubble': "url('screens/home/images/bubble-light-green.png')",
        'hero-right': "url('screens/home/images/hero-bg-right.png')",
        'bubble-group': "url('screens/home/images/bubble-light-group.png')",
        'bubble-dark-group': "url('screens/home/images/bubble-dark-group.png')",
        'bubble-big-green': "url('screens/home/images/bubble-big-green.png')",
        'header-person': "url('screens/home/images/header-person.png')"
      },
      backgroundSize: {
        '1.2': '120%',
        '24': '24rem',
        '32': '32rem',
        '48': '48rem',
        '64': '64rem',
      },
      colors: {
        green: {
          100: '#eaf1ea',
          700: '#7a9f7d',
          900: '#2c442c',
        },
        yellow: {
          150: '#ffede3',
        }
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
